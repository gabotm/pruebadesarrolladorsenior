﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace WebApi.Controllers
{
    public class HomeController : Controller
    {
        SqlConnection sqlConnection = new SqlConnection("Server=DEV01;Database=StoreSample;Trusted_Connection=True;");
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        [HttpGet]
        public JsonResult SalesDatePrediction()
        {
            List<dynamic> orders = new List<dynamic>();

            try
            {
                sqlConnection.Open();
                string query = @"SELECT 
                                o.custid,
	                            c.companyname AS CustomerName,
	                            MAX(o.orderdate) AS LastOrderDate,
	                            CASE WHEN (count(*) - 1) > 0 THEN 
		                            DATEADD(DAY, DATEDIFF(DAY, min(o.orderdate), max(o.orderdate)) / (count(*) - 1), max(o.orderdate))
	                            ELSE 
		                            DATEADD(DAY, DATEDIFF(DAY, min(o.orderdate), max(o.orderdate)) / count(*), max(o.orderdate))
	                            END AS NextPredictedOrder
                              FROM [Sales].[Orders] AS o
                              INNER JOIN [Sales].Customers AS c ON c.custid = o.custid
                              GROUP BY o.custid, c.companyname";

                SqlCommand command = new SqlCommand(query, sqlConnection);

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var order = new Dictionary<string, dynamic>();
                    order["CustomerId"] = reader.GetInt32(0);
                    order["CustomerName"] = reader.GetString(1);
                    order["LastOrderDate"] = reader.GetSqlValue(2).ToString();
                    order["NextPredictedOrder"] = reader.GetSqlValue(3).ToString();
              
                    orders.Add(order);
                }


            } catch(Exception ex)
            {

            }
            return Json(orders, JsonRequestBehavior.AllowGet);
            
        }

        [HttpGet]
        public JsonResult GetClientOrders(int custId)
        {
            List<dynamic> orders = new List<dynamic>();

            try
            {
                sqlConnection.Open();
                string query = @"SELECT [orderid]    
                                  ,[requireddate]
                                  ,[shippeddate]
                                  ,[shipname]
                                  ,[shipaddress]
                                  ,[shipcity]
                              FROM [Sales].[Orders]
                              WHERE [custid] = " + custId;

                SqlCommand command = new SqlCommand(query, sqlConnection);

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var order = new Dictionary<string, dynamic>();
                    order["id"] = reader.GetInt32(0);
                    order["requiredDate"] = reader.GetSqlValue(1).ToString();
                    order["shippedDate"] = reader.GetSqlValue(2).ToString();
                    order["shipName"] = reader.GetString(3);
                    order["shipAddress"] = reader.GetString(4);
                    order["shipCity"] = reader.GetString(5);

                    orders.Add(order);
                }


            }
            catch (Exception ex)
            {

            }
            return Json(orders, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult GetEmployees()
        {
            List<dynamic> employees = new List<dynamic>();

            try
            {
                sqlConnection.Open();
                string query = @"SELECT [empid],
                                      CONCAT([firstname], ' ', [lastname]) AS FullName
                                  FROM [HR].[Employees] ";

                SqlCommand command = new SqlCommand(query, sqlConnection);

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var employee = new Dictionary<string, dynamic>();
                    employee["EmpId"] = reader.GetInt32(0);
                    employee["FullName"] = reader.GetString(1);

                    employees.Add(employee);
                }


            }
            catch (Exception ex)
            {

            }
            return Json(employees, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult GetShippers()
        {
            List<dynamic> shippers = new List<dynamic>();

            try
            {
                sqlConnection.Open();
                string query = @"SELECT 
	                            [shipperid],
	                            [companyname]
                              FROM [Sales].[Shippers]";

                SqlCommand command = new SqlCommand(query, sqlConnection);

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var shipper = new Dictionary<string, dynamic>();
                    shipper["ShipperId"] = reader.GetInt32(0);
                    shipper["CompanyName"] = reader.GetString(1);

                    shippers.Add(shipper);
                }


            }
            catch (Exception ex)
            {

            }
            return Json(shippers, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult GetProducts()
        {
            List<dynamic> products = new List<dynamic>();

            try
            {
                sqlConnection.Open();
                string query = @"SELECT 
	                            [productid],
	                            [productname]
                              FROM [Production].[Products]";

                SqlCommand command = new SqlCommand(query, sqlConnection);

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var product = new Dictionary<string, dynamic>();
                    product["ProductId"] = reader.GetInt32(0);
                    product["ProductName"] = reader.GetString(1);

                    products.Add(product);
                }


            }
            catch (Exception ex)
            {

            }
            return Json(products, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult AddNewOrder(dynamic order)
        {
            var result = 0;
            try
            {
                sqlConnection.Open();
                string query = @"
                    DECLARE @employeeId AS INT = " + order.employeeId + @";
                    DECLARE @shipperId AS INT = " + order.shipperId + @";
                    DECLARE @shipName AS VARCHAR(100) = '" + order.shippName + @"';
                    DECLARE @shipAddress AS VARCHAR(100) = '" + order.shipAddress + @"';
                    DECLARE @shipCity AS VARCHAR(100) = '" + order.shipCity + @"';
                    DECLARE @orderDate AS SMALLDATETIME = GETDATE();
                    DECLARE @requiredDate AS SMALLDATETIME = '" + order.requiredDate + @" 00:00:00';
                    DECLARE @shippedDate AS SMALLDATETIME = '" + order.shippedDate + @" 00:00:00';
                    DECLARE @freight AS DECIMAL = " + order.freight + @";
                    DECLARE @shipCountry AS VARCHAR(100) = '" + order.shipCountry + @"';

                    DECLARE @orderId AS INT = 0;
                    DECLARE @productId AS INT = " + order.productId + @";
                    DECLARE @unitPrice AS DECIMAL = (SELECT unitprice FROM Production.Products WHERE productid = @productId);
                    DECLARE @quantity AS INT = " + order.quantity + @";
                    DECLARE @discount AS DECIMAL = " + order.discount + @";

                    BEGIN TRANSACTION
	                    INSERT INTO [Sales].[Orders]
			                        ([empid]
			                        ,[orderdate]
			                        ,[requireddate]
			                        ,[shippeddate]
			                        ,[shipperid]
			                        ,[freight]
			                        ,[shipname]
			                        ,[shipaddress]
			                        ,[shipcity]
			                        ,[shipcountry])
		                        VALUES
			                        (@employeeId
			                        ,@orderDate
			                        ,@requiredDate
			                        ,@shippedDate
			                        ,@shipperId
			                        ,@freight
			                        ,@shipName
			                        ,@shipAddress
			                        ,@shipCity
			                        ,@shipCountry);


                    SET @orderId = SCOPE_IDENTITY();

                    IF @orderId > 0 
	                    BEGIN
		                    INSERT INTO [Sales].[OrderDetails]
			                        ([orderid]
			                        ,[productid]
			                        ,[unitprice]
			                        ,[qty]
			                        ,[discount])
		                        VALUES
			                        (@orderId
			                        ,@productId
			                        ,@unitPrice
			                        ,@quantity
			                        ,@discount)
		                    COMMIT TRANSACTION;
	                    END
                    ELSE
	                    ROLLBACK TRANSACTION;";

                SqlCommand command = new SqlCommand(query, sqlConnection);

                result = command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
