-- Add new Order
USE [StoreSample]

DECLARE @employeeId AS INT = 4;
DECLARE @shipperId AS INT = 1;
DECLARE @shipName AS VARCHAR(100) = '80-D';
DECLARE @shipAddress AS VARCHAR(100) = 'Cra 80D #7B-83';
DECLARE @shipCity AS VARCHAR(100) = 'Bogota';
DECLARE @orderDate AS SMALLDATETIME = GETDATE();
DECLARE @requiredDate AS SMALLDATETIME = '20220829 00:00:00';
DECLARE @shippedDate AS SMALLDATETIME = '20220830 00:00:00';
DECLARE @freight AS DECIMAL = 29.5;
DECLARE @shipCountry AS VARCHAR(100) = 'Colombia';

DECLARE @orderId AS INT = 0;
DECLARE @productId AS INT = 4;
DECLARE @unitPrice AS DECIMAL = (SELECT unitprice FROM Production.Products WHERE productid = @productId);
DECLARE @quantity AS INT = 2;
DECLARE @discount AS DECIMAL = 0.0;

BEGIN TRANSACTION
	INSERT INTO [Sales].[Orders]
			   ([empid]
			   ,[orderdate]
			   ,[requireddate]
			   ,[shippeddate]
			   ,[shipperid]
			   ,[freight]
			   ,[shipname]
			   ,[shipaddress]
			   ,[shipcity]
			   ,[shipcountry])
		 VALUES
			   (@employeeId
			   ,@orderDate
			   ,@requiredDate
			   ,@shippedDate
			   ,@shipperId
			   ,@freight
			   ,@shipName
			   ,@shipAddress
			   ,@shipCity
			   ,@shipCountry);


SET @orderId = SCOPE_IDENTITY();

IF @orderId > 0 
	BEGIN
		INSERT INTO [Sales].[OrderDetails]
			   ([orderid]
			   ,[productid]
			   ,[unitprice]
			   ,[qty]
			   ,[discount])
		 VALUES
			   (@orderId
			   ,@productId
			   ,@unitPrice
			   ,@quantity
			   ,@discount)
		COMMIT TRANSACTION;
	END
ELSE
	ROLLBACK TRANSACTION;
