import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Order } from './order';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  httpHeader = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin' : 'http://localhost:4200',
    })
  }  

  public salesDatePrediction(){
    return this.httpClient.get(`https://localhost:44378/Home/SalesDatePrediction`);
  }

  public getClientOrders(custId:any){
    return this.httpClient.get(`https://localhost:44378/Home/GetClientOrders?custId=`+custId);
  }

  public getEmployees(){
    return this.httpClient.get(`https://localhost:44378/Home/GetEmployees`);
  }

  public getShippers(){
    return this.httpClient.get(`https://localhost:44378/Home/GetShippers`);
  }

  public getProducts(){
    return this.httpClient.get(`https://localhost:44378/Home/GetProducts`);
  }

  public newOrder(order: any) {
    return this.httpClient.post<any>(`https://localhost:44378/Home/AddNewOrder`, order, this.httpHeader);
  }
  
}
