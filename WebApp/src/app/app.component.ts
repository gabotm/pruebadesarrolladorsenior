import { Component } from '@angular/core';
import { ApiService } from './api.service';
import { FormGroup, FormControl, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { Order } from './order';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Sales Prediction App';
  ordersList: any;
  pagesOL: number = 1;
  pagesCOL: number = 1;
  clientOrdersList: any;
  clientName: string='';
  employeeList: any;
  shipperList: any;
  productList: any;
  neworder!: Order;
  dataSaved = false;
  orderForm = new FormGroup({
    employeeId: new FormControl('', [Validators.required]),
    shipperId: new FormControl('', [Validators.required]),
    shipName: new FormControl('', [Validators.required]),
    shipAddress: new FormControl('', [Validators.required]),
    shipCity: new FormControl('', [Validators.required]),
    orderDate: new FormControl('', [Validators.required]),
    requiredDate: new FormControl('', [Validators.required]),
    shippedDate: new FormControl('', [Validators.required]),
    freight: new FormControl('', [Validators.required]),
    shipCountry: new FormControl('', [Validators.required]),
    productId: new FormControl('', [Validators.required]),
    unitePrice: new FormControl('', [Validators.required]),
    quantity: new FormControl('', [Validators.required]),
    discount: new FormControl('', [Validators.required])
  });
  
  constructor(private apiService: ApiService) {}
  
  ngOnInit() {
    this.apiService.salesDatePrediction().subscribe((data)=>{
      this.ordersList = data;
    });
  }

  _getClientOrders(_cname: string, _cid: any) {
    this.apiService.getClientOrders(_cid).subscribe((data)=>{
      this.clientOrdersList = data;
      this.clientName = _cname;
    });
  }

  _createNewOrder(_cname: string, _cid: number) {
    this.clientName = _cname;
    this.apiService.getEmployees().subscribe((data)=>{ this.employeeList = data;  });
    this.apiService.getShippers().subscribe((data)=>{ this.shipperList = data;  });
    this.apiService.getProducts().subscribe((data)=>{ this.productList = data;  });
  }

  onFormSubmit() {
    this.dataSaved = false;
    console.log(this.orderForm.value);
    let order = this.orderForm.value;
    
    this.apiService.newOrder(order).subscribe((data)=>{ this.dataSaved = true; });
  }

}