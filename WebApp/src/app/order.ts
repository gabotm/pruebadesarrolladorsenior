export interface Order {
    employeeId: string;
    shipperId: string;
    shipName: string;
    shipAddress: string;
    shipCity: string;
    requiredDate: string;
    shippedDate: string;
    freight: string;
    shipCountry: string;
    productId: string;
    quantity: string;
    discount: string;
} 