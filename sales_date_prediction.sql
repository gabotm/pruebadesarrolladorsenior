-- Sales Date Prediction
SELECT 
	c.companyname AS CustomerName,
	MAX(o.orderdate) AS LastOrderDate,
	CASE WHEN (count(*) - 1) > 0 THEN 
		DATEADD(DAY, DATEDIFF(DAY, min(o.orderdate), max(o.orderdate)) / (count(*) - 1), max(o.orderdate))
	ELSE 
		DATEADD(DAY, DATEDIFF(DAY, min(o.orderdate), max(o.orderdate)) / count(*), max(o.orderdate))
	END AS NextPredictedOrder
  FROM [StoreSample].[Sales].[Orders] AS o
  INNER JOIN [StoreSample].[Sales].Customers AS c ON c.custid = o.custid
  GROUP BY o.custid, c.companyname